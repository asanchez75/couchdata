<?php

namespace Drupal\couchdata\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the couchdata module.
 */
class DefaultControllerTest extends WebTestBase {
  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => "couchdata DefaultController's controller functionality",
      'description' => 'Test Unit for module couchdata and controller DefaultController.',
      'group' => 'Other',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests couchdata functionality.
   */
  public function testDefaultController() {
    // Check that the basic functions of module couchdata.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via App Console.');
  }

}
