<?php

namespace Drupal\couchdata;
use PHPOnCouch\CouchClient;

/**
 * Class CouchDBService.
 *
 * @package Drupal\couchdata
 */
class CouchDBService {
  /**
   * Constructor.
   */

  public $client;

  public function __construct() {
      $client = new CouchClient('http://admin:holaperu@45.79.10.52:5984', 'sig_demo');
      $this->client = $client;
  }

  public function getView($id, $viewname) {
      try {
          return $view = $this->client->limit(100000)->getView($id, $viewname);
      } catch (Exception $e) {
          dpm("something weird happened: " . $e->getMessage());
      }
  }

}
