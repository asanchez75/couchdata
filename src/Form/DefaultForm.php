<?php

namespace Drupal\couchdata\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DefaultForm.
 *
 * @package Drupal\couchdata\Form
 */
class DefaultForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'couchdata';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
      $content_types = array_keys(node_type_get_types());

      $form['node_type'] = [
          '#type' => 'select',
          '#title' => $this->t('Content types'),
          '#description' => $this->t('Select content type to annotate'),
          '#options' => array_combine($content_types, $content_types),
      ];
      // Group submit handlers in an actions element with a key of "actions" so
      // that it gets styled correctly, and so that other modules may add actions
      // to the form. This is not required, but is convention.
      $form['actions'] = [
          '#type' => 'actions',
      ];

      // Add a submit button that handles the submission of the form.
      $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Submit'),
      ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      $node_type = $form_state->getValue('node_type');

      $batch = array(
          'title' => t('Exporting'),
          'operations' => array(
              array('couchdata_batch', array($node_type)),
          ),
          'finished' => 'couchdata_batch_finished_callback',
          'file' => drupal_get_path('module', 'couchdata') . '/couchdata.batch.inc',
      );

      batch_set($batch);

  }


}
