<?php

namespace Drupal\couchdata;
use Drupal\Component\Serialization\Json;
use Drupal\node\Entity\Node;
use \Drupal\Core\Form;
/**
 * Class ContentService.
 *
 * @package Drupal\couchdata
 */
class ContentService {
  /**
   * Constructor.
   */
  public function __construct() {

  }

  public function SaveProductor($row) {
      if ($row->type == 'productor') {
          $node = Node::create([
              'type' => $row->type,
              'langcode' => 'en',
              'created' => REQUEST_TIME,
              'changed' => REQUEST_TIME,
              'uid' => 1,
              'title' => $row->apellidos . ' ' . $row->nombres,
              'field_apellidos' => ['value' => $row->apellidos],
              'field_nombres' => ['value' => $row->nombres],
              'field_caser' => ['value' => $row->caserio],
              'field_celular' => ['value' => $row->celular],
              'field_comite_bae' => ['value' => $row->comite_base],
              'field_conyuge_apellidos' => ['value' => $row->conyuge_apellidos],
              'field_conyuge_dni' => ['value' => $row->conyuge_dni],
              'field_conyuge_nombres' => ['value' => $row->conyuge_nombres],
              'field_distrito' => ['value' => $row->distrito],
              'field_dni' => ['value' => $row->dni],
              'field_e_mail' => ['value' => $row->email],
              'field_estado_civil' => ['value' => $row->estado_civil],
              'field_fecha_de_nacimiento' => ['value' => date_format(date_create($row->fechanac), 'd/m/Y')],
              'field_grado_de_instruccion' => ['value' => $row->grado_de_instruccion],
              'field_organizacion_ano_ingreso' => ['value' => $row->organizacion_anio_ingreso],
              'field_organizacion_folio_registr' => ['value' => $row->organizacion_folio_registro],
              'field_provincia' => ['value' => $row->provincia],
              'field_region' => ['value' => $row->region],
              'field_sexo' => ['value' => $row->sexo],
          ]);
          $node->save();
      }
      else {
          drupal_set_message('The node ' . $row->dni . ' is not a productor');
      }

  }

    public function SaveParcela($row) {
        if ($row->type == 'parcela') {
            try {
                $node = Node::create([
                    'type' => $row->type,
                    'langcode' => 'en',
                    'created' => REQUEST_TIME,
                    'changed' => REQUEST_TIME,
                    'uid' => 1,
                    'title' => $row->nombre,
                    'field_altitud' => ['value' => $row->altitud],
                    'field_bosque_primario' => ['value' => $row->bosque_primario],
                    'field_cafe_crecimiento' => ['value' => $row->cafe_crecimiento],
                    'field_cafe_produccion' => ['value' => $row->cafe_produccion],
                    'field_caser' => ['value' => $row->caserio],
                    'field_coordenadas' => ['value' => $row->coordenadas],
                    'field_cuenta_analisis_suelo' => ['value' => $row->cuenta_analisis_suelo],
                    'field_estatus_actual' => ['value' => $row->estatus_actual],
                    'field_estatus_anterior' => ['value' => $row->estatus_anterior],
                    'field_inverna_pasto' => ['value' => $row->inverna_pasto],
                    // this a multiple value
                    'field_normas_estandares' => ['value' => implode(',', $row->normas_estandares)],
                    'field_otros_cultivos' => ['value' => $row->otros_cultivos],
                    'field_pendiente_promedio' => ['value' => $row->pendiente_promedio],
                    'field_produccion_compromiso_acop' => ['value' => $row->produccion_compromiso_acopio],
                    'field_produccion_cosecha_actual' => ['value' => $row->produccion_cosecha_actual],
                    'field_produccion_cosecha_anterio' => ['value' => $row->produccion_cosecha_anterior],
                    'field_produccion_unidad_medida' => ['value' => $row->produccion_unidad_medida],
                    'field_productor' => ['value' => $row->productor],
                    'field_rastrojo_purma' => ['value' => $row->rastrojo_purma],
                    'field_reserva' => ['value' => $row->reserva],
                    'field_sector' => ['value' => $row->sector],
                    'field_sombra_edad_promedio' => ['value' => $row->sombra_edad_promedio],
                    // this a multiple value
                    'field_sombra_variedades' => ['value' => implode(',', $row->sombra_variedades)],
                    'field_sombra_promedio' => ['value' => $row->sombra_promedio],
                    'field_suelo_realizo_anios' => ['value' => $row->suelo_realizo_anios],
                    // this a multiple value
                    'field_tenencia' => array('value' => implode(',', $row->tenencia)),
                    'field_tipo1_cantidad' => ['value' => $row->tipo1_cantidad],
                    'field_tipo1_edad' => ['value' => Json::encode($row->tipo1_edad)],
                    'field_tipo1_va' => ['value' => $row->tipo1_variedad],
                    'field_tipo1_anotaciones' => ['value' => $row->tipo1_anotaciones],
                    'field_tipo2_cant' => ['value' => $row->tipo2_cantidad],
                    'field_tipo2_edad' => ['value' => Json::encode($row->tipo2_edad)],
                    'field_tipo2_variedad' => ['value' => $row->tipo2_variedad],
                    'field_tipo2_anotaciones' => ['value' => $row->tipo2_anotaciones],
                    'field_tipo3_cantidad' => ['value' => $row->tipo3_cantidad],
                    'field_tipo3_edad' => ['value' => Json::encode($row->tipo3_edad)],
                    'field_tipo3_vari' => ['value' => $row->tipo3_variedad],
                    'field_tipo3_anotaciones' => ['value' => $row->tipo3_anotaciones],
                    'field_tipo4_cantidad' => ['value' => $row->tipo4_cantidad],
                    'field_tipo4_edad' => ['value' => Json::encode($row->tipo4_edad)],
                    'field_tipo4_varida' => ['value' => $row->tipo4_variedad],
                    'field_tipo4_anotaciones' => ['value' => $row->tipo4_anotaciones],
                    'field_tipo5_ca' => ['value' => $row->tipo5_cantidad],
                    'field_tipo5_edad' => ['value' => Json::encode($row->tipo5_edad)],
                    'field_tipo5_varid' => ['value' => $row->tipo5_variedad],
                    'field_tipo5_anotaciones' => ['value' => $row->tipo5_anotaciones],
                    'field_tipo_analisis_suelo' => ['value' => $row->tipo_analisis_suelo],
                    'field_otros_cultivos_especificar' => ['value' => implode(',', $row->especificar_otros_cultivos)],
                    'field_tipo_crecimiento' => ['value' => $row->tipo_crecimiento],
                    'field_practicas_de_conservacion' => ['value' => implode(',', $row->practicas_conservacion)],
                    'field_areas_de_practicas' => ['value' => $row->areas_practicas],
                    'field_parcela_key' => ['value' => $row->_id]
                ]);
                $node->save();
            }
            catch (\Exception $e) {
                drupal_set_message($e->getMessage());
                dpm($row);
            }
        }
        else {
            drupal_set_message('The node ' . $row->dni . ' is not a parcela');
        }
    }

    public function SaveObservacion($row) {
        if ($row->type == 'observacion') {
            try {
                $node = Node::create([
                    'type' => $row->type,
                    'langcode' => 'en',
                    'created' => REQUEST_TIME,
                    'changed' => REQUEST_TIME,
                    'uid' => 1,
                    'title' => $row->pregunta_value,
                    'field_descripcion_encontradas' => ['value' => $row->descripcion_encontradas],
                    'field_descripcion_propuestas' => ['value' => $row->descripcion_propuestas],
                    'field_inspeccion' => ['value' => $row->inspeccion],
                    'field_plazo_cumplimiento' => ['value' => date_format(date_create($row->plazo_cumplimiento), 'd/m/Y')],
                    'field_pregunta_key' => ['value' => $row->pregunta_key],
                    'field_pregunta_value' => ['value' => $row->pregunta_value]
                ]);
                $node->save();
            } catch (\Exception  $e) {
                drupal_set_message($e->getMessage());
                dpm($row);
            }
        }
        else {
            drupal_set_message('The node ' . $row->dni . ' is not an observacion');
        }

    }

    public function SaveInspeccion($row) {
        if ($row->type == 'inspeccion') {
            try {
                $node = Node::create([
                    'type' => $row->type,
                    'langcode' => 'en',
                    'created' => REQUEST_TIME,
                    'changed' => REQUEST_TIME,
                    'uid' => 1,
                    'title' => $row->parcela,
                    'field_parcela_key' => ['value' => $row->parcela],
                    'field_inspeccion_key' => ['value' => $row->_id],
                    'field_uno_evaluacion' => ['value' => $row->uno_evaluacion],
                    'field_uno_observaciones' => ['value' => $row->uno_observaciones],
                    'field_dos_evaluacion' => ['value' => $row->dos_evaluacion],
                    'field_dos_observaciones' => ['value' => $row->dos_observaciones],
                    'field_tres_evaluacion' => ['value' => $row->tres_evaluacion],
                    'field_tres_observaciones' => ['value' => $row->tres_observaciones],
                    'field_cuatro_evaluacion' => ['value' => $row->cuatro_evaluacion],
                    'field_cuatro_observaciones' => ['value' => $row->cuatro_observaciones],
                    'field_cinco_evaluacion' => ['value' => $row->cinco_evaluacion],
                    'field_cinco_observaciones' => ['value' => $row->cinco_observaciones],
                    'field_seis_evaluacion' => ['value' => $row->seis_evaluacion],
                    'field_seis_observaciones' => ['value' => $row->seis_observaciones],
                    'field_siete_evaluacion' => ['value' => $row->siete_evaluacion],
                    'field_siete_ejecucion' => ['value' => $row->siete_ejecucion],
                    'field_ocho_evaluacion' => ['value' => $row->ocho_evaluacion],
                    'field_ocho_areas_productivas' => ['value' => $row->ocho_areas_productivas],
                    'field_ocho_areas_conservacion' => ['value' => $row->ocho_areas_conservacion],
                    'field_ocho_zonas_riesgos' => ['value' => $row->ocho_zonas_riesgos],
                    'field_ocho_cuerpos_agua' => ['value' => $row->ocho_cuerpos_agua],
                    'field_ocho_viviendas' => ['value' => $row->ocho_viviendas],
                    'field_ocho_infraestructura' => ['value' => $row->ocho_infraestructura],
                    'field_nueve_evaluacion' => ['value' => $row->nueve_evaluacion],
                    'field_nueve_numero_eventos' => ['value' => $row->nueve_numero_eventos],
                    'field_nueve_de_organizados' => ['value' => $row->nueve_de_organizados],
                    'field_diez_evaluacion' => ['value' => $row->diez_evaluacion],
                    'field_diez_observaciones' => ['value' => $row->diez_observaciones],
                    'field_once_evaluacion' => ['value' => $row->once_evaluacion],
                    'field_once_abonamiento' => ['value' => $row->once_abonamiento],
                    'field_once_cosecha' => ['value' => $row->once_cosecha],
                    'field_once_ventas_cafe' => ['value' => $row->once_ventas_cafe],
                    'field_once_otras_labores' => ['value' => $row->once_otras_labores],
                    'field_doce_evaluacion' => ['value' => $row->doce_evaluacion],
                    'field_doce_guia_recepcion' => ['value' => $row->doce_guia_recepcion],
                    'field_doce_liquidacion' => ['value' => $row->doce_liquidacion],
                    'field_trece_evaluacion' => ['value' => $row->trece_evaluacion],
                    'field_trece_boleta_de_venta' => ['value' => $row->trece_boleta_de_venta],
                    'field_trece_planillas' => ['value' => $row->trece_planillas],
                    'field_trece_otro' => ['value' => $row->trece_otro],
                    'field_trece_especificar' => ['value' => $row->trece_especificar],
                    'field_catorce_evaluacion' => ['value' => $row->catorce_evaluacion],
                    'field_catorce_zonas_amortiguamie' => ['value' => $row->catorce_zonas_amortiguamiento],
                    'field_catorce_otros' => ['value' => $row->catorce_otros],
                    'field_catorce_especificar' => ['value' => $row->catorce_especificar],
                    'field_quince_evaluacion' => ['value' => $row->quince_evaluacion],
                    'field_quince_nombre' => ['value' => $row->quince_nombre],
                    'field_quince_procedencia' => ['value' => $row->quince_procedencia],
                    'field_dieciseis_evaluacion' => ['value' => $row->dieciseis_evaluacion],
                    'field_dieciseis_describir' => ['value' => $row->dieciseis_describir],
                    'field_diecisiete_evaluacion' => ['value' => $row->diecisiete_evaluacion],
                    'field_diecisiete_variedades' => ['value' => $row->diecisiete_variedades],
                    'field_diecisiete_procedencia' => ['value' => $row->diecisiete_procedencia],
                    'field_dieciocho_evaluacion' => ['value' => $row->dieciocho_evaluacion],
                    'field_dieciocho_procedencia' => ['value' => $row->dieciocho_procedencia],
                    'field_dieciocho_variedades' => ['value' => $row->dieciocho_variedades],
                    'field_diecinueve_evaluacion' => ['value' => $row->diecinueve_evaluacion],
                    'field_diecinueve_nombre' => ['value' => $row->diecinueve_nombre],
                    'field_diecinueve_cantidad' => ['value' => $row->diecinueve_cantidad],
                    'field_diecinueve_dosis' => ['value' => $row->diecinueve_dosis],
                    'field_diecinueve_asesoramiento' => ['value' => $row->diecinueve_asesoramiento],
                    'field_veinte_evaluacion' => ['value' => $row->veinte_evaluacion],
                    'field_veinte_pulpa_cafe' => ['value' => $row->veinte_pulpa_cafe],
                    'field_veinte_ceniza' => ['value' => $row->veinte_ceniza],
                    'field_veinte_restos_organicos' => ['value' => $row->veinte_restos_organicos],
                    'field_veinte_estiercol' => ['value' => $row->veinte_estiercol],
                    'field_veinte_restos_vegetales' => ['value' => $row->veinte_restos_vegetales],
                    'field_veinte_otros' => ['value' => $row->veinte_otros],
                    'field_veinte_especificar' => ['value' => $row->veinte_especificar],
                    'field_veintiuno_evaluacion' => ['value' => $row->veintiuno_evaluacion],
                    'field_veintiuno_observaciones' => ['value' => $row->veintiuno_observaciones],
                    'field_veintidos_evaluacion' => ['value' => $row->veintidos_evaluacion],
                    'field_veintidos_observaciones' => ['value' => $row->veintidos_observaciones],
                    'field_veintitres_evaluacion' => ['value' => $row->veintitres_evaluacion],
                    'field_veintitres_tipo' => ['value' => $row->veintitres_tipo],
                    'field_veintitres_ordenada' => ['value' => $row->veintitres_ordenada],
                    'field_veintitres_planificada' => ['value' => $row->veintitres_planificada],
                    'field_veinticuatro_evaluacion' => ['value' => $row->veinticuatro_evaluacion],
                    'field_veinticuatro_tipo' => ['value' => $row->veinticuatro_tipo],
                    'field_veinticuatro_ordenada' => ['value' => $row->veinticuatro_ordenada],
                    'field_veinticuatro_planificada' => ['value' => $row->veinticuatro_planificada],
                    'field_veinticinco_evaluacion' => ['value' => $row->veinticinco_evaluacion],
                    'field_veinticinco_crianzas' => ['value' => $row->veinticinco_crianzas],
                    'field_veinticinco_describa' => ['value' => $row->veinticinco_describa],
                    'field_veintiseis_evaluacion' => ['value' => $row->veintiseis_evaluacion],
                    'field_veintiseis_hortalizas' => ['value' => $row->veintiseis_hortalizas],
                    'field_veintiseis_frutales' => ['value' => $row->veintiseis_frutales],
                    'field_veintisiete_evaluacion' => ['value' => $row->veintisiete_evaluacion],
                    'field_veintisiete_describa' => ['value' => $row->veintisiete_describa],
                    'field_veintiocho_evaluacion' => ['value' => $row->veintiocho_evaluacion],
                    'field_veintiocho_describa' => ['value' => $row->veintiocho_describa],
                    'field_veintinueve_evaluacion' => ['value' => $row->veintinueve_evaluacion],
                    'field_veintinueve_describa' => ['value' => $row->veintinueve_describa],
                    'field_treinta_evaluacion' => ['value' => $row->treinta_evaluacion],
                    'field_treinta_evaluacion_despulp' => ['value' => $row->treinta_despulpadora],
                    'field_treinta_evaluacion_tanque_' => ['value' => $row->treinta_tanque_tina],
                    'field_treinta_evaluacion_otro' => ['value' => $row->treinta_otro],
                    'field_treinta_evaluacion_especif' => ['value' => $row->treinta_especificar],
                    'field_treintaiuno_evaluacion' => ['value' => $row->treintaiuno_evaluacion],
                    'field_treintaiuno_secado_solar' => ['value' => $row->treintaiuno_secado_solar],
                    'field_treintaiuno_modulo_secado' => ['value' => $row->treintaiuno_modulo_secado],
                    'field_treintaiuno_terrado' => ['value' => $row->treintaiuno_terrado],
                    'field_treintaiuno_pampillo_cemen' => ['value' => $row->treintaiuno_pampillo_cemento],
                    'field_treintaiuno_otros' => ['value' => $row->treintaiuno_otros],
                    'field_treintaiuno_especificar' => ['value' => $row->treintaiuno_especificar],
                    'field_treintaidos_evaluacion' => ['value' => $row->treintaidos_evaluacion],
                    'field_treintaidos_ventilado' => ['value' => $row->treintaidos_ventilado],
                    'field_treintaidos_tarimas' => ['value' => $row->treintaidos_tarimas],
                    'field_treintaidos_separado_pared' => ['value' => $row->treintaidos_separado_paredes],
                    'field_treintaidos_separado_conta' => ['value' => $row->treintaidos_separado_contaminantes],
                    'field_treintaitres_evaluacion' => ['value' => $row->treintaitres_evaluacion],
                    'field_treintaitres_tipo_instalac' => ['value' => $row->treintaitres_tipo_instalacion],
                    'field_treintaitres_tipo_manejo' => ['value' => $row->treintaitres_tipo_manejo],
                    'field_treintaicuatro_evaluacion' => ['value' => $row->treintaicuatro_evaluacion],
                    'field_treintaicuatro_tipo_instal' => ['value' => $row->treintaicuatro_tipo_instalacion],
                    'field_treintaicuatro_tipo_manejo' => ['value' => $row->treintaicuatro_tipo_manejo],
                    'field_treintaicinco_evaluacion' => ['value' => $row->treintaicinco_evaluacion],
                    'field_treintaicinco_tipo_instala' => ['value' => $row->treintaicinco_tipo_instalacion],
                    'field_treintaicinco_manejo' => ['value' => $row->treintaicinco_manejo],
                    'field_treintaiseis_evaluacion' => ['value' => $row->treintaiseis_evaluacion],
                    'field_treintaiseis_tipo_instalac' => ['value' => $row->treintaiseis_tipo_instalacion],
                    'field_treintaiseis_tipo_manejo' => ['value' => $row->treintaiseis_tipo_manejo],
                    'field_treintaisiete_evaluacion' => ['value' => $row->treintaisiete_evaluacion],
                    'field_treintaisiete_especies' => ['value' => $row->treintaisiete_especies],
                    'field_treintaisiete_describa' => ['value' => $row->treintaisiete_describa],
                    'field_treintaiocho_evaluacion' => ['value' => $row->treintaiocho_evaluacion],
                    'field_treintaiocho_describa' => ['value' => $row->treintaiocho_describa],
                    'field_treintainueve_evaluacion' => ['value' => $row->treintainueve_evaluacion],
                    'field_treintainueve_conserva_hab' => ['value' => $row->treintainueve_conserva_habitat],
                    'field_treintainueve_rotulos' => ['value' => $row->treintainueve_rotulos],
                    'field_treintainueve_otros' => ['value' => $row->treintainueve_otros],
                    'field_treintainueve_especificar' => ['value' => $row->treintainueve_especificar],
                    'field_treintainueve_acciones_sen' => ['value' => $row->treintainueve_acciones_sensibilizacion],
                    'field_treintainueve_describa' => ['value' => $row->treintainueve_describa],
                    'field_cuarenta_evaluacion' => ['value' => $row->cuarenta_evaluacion],
                    'field_cuarenta_describa' => ['value' => $row->cuarenta_describa],
                    'field_cuarentaiuno_evaluacion' => ['value' => $row->cuarentaiuno_evaluacion],
                    'field_cuarentaiuno_describa' => ['value' => $row->cuarentaiuno_describa],
                    'field_cuarentaidos_evaluacion' => ['value' => $row->cuarentaidos_evaluacion],
                    'field_cuarentaidos_barreras' => ['value' => $row->cuarentaidos_barreras],
                    'field_cuarentaidos_coberturas_vi' => ['value' => $row->cuarentaidos_coberturas_vivas],
                    'field_cuarentaidos_coberturas_mu' => ['value' => $row->cuarentaidos_coberturas_muertas],
                    'field_cuarentaidos_incorporacion' => ['value' => $row->cuarentaidos_incorporacion_materia],
                    'field_cuarentaidos_otros' => ['value' => $row->cuarentaidos_otros],
                    'field_cuarentaidos_especificar' => ['value' => $row->cuarentaidos_especificar],
                    'field_cuarentaitres_evaluacion' => ['value' => $row->cuarentaitres_evaluacion],
                    'field_cuarentaitres_zonas_amorti' => ['value' => $row->cuarentaitres_zonas_amortiguamiento],
                    'field_cuarentaitres_reforestacio' => ['value' => $row->cuarentaitres_reforestacion],
                    'field_cuarentaitres_acciones_sen' => ['value' => $row->cuarentaitres_acciones_sensibilizacion],
                    'field_cuarentaitres_especificar' => ['value' => $row->cuarentaitres_especificar],
                    'field_cuarentaicuatro_evaluacion' => ['value' => $row->cuarentaicuatro_evaluacion],
                    'field_cuarentaicuatro_solidos' => ['value' => $row->cuarentaicuatro_solidos],
                    'field_cuarentaicuatro_liquidos' => ['value' => $row->cuarentaicuatro_liquidos],
                    'field_cuarentaicinco_evaluacion' => ['value' => $row->cuarentaicinco_evaluacion],
                    'field_cuarentaicinco_tanque_quin' => ['value' => $row->cuarentaicinco_tanque_quina],
                    'field_cuarentaicinco_reservorio' => ['value' => $row->cuarentaicinco_reservorio],
                    'field_cuarentaicinco_cosecha_agu' => ['value' => $row->cuarentaicinco_cosecha_agua],
                    'field_cuarentaicinco_instalacion' => ['value' => $row->cuarentaicinco_instalaciones_fugas],
                    'field_cuarentaicinco_otros' => ['value' => $row->cuarentaicinco_otros],
                    'field_cuarentaicinco_especificar' => ['value' => $row->cuarentaicinco_especificar],
                    'field_cuarentaiseis_evaluacion' => ['value' => $row->cuarentaiseis_evaluacion],
                    'field_cuarentaiseis_agua_hervida' => ['value' => $row->cuarentaiseis_agua_hervida],
                    'field_cuarentaiseis_otros' => ['value' => $row->cuarentaiseis_otros],
                    'field_cuarentaiseis_especificar' => ['value' => $row->cuarentaiseis_especificar],
                    'field_cuarentaisiete_evaluacion' => ['value' => $row->cuarentaisiete_evaluacion],
                    'field_cuarentaisiete_describa' => ['value' => $row->cuarentaisiete_describa],
                    'field_cuarentaiocho_evaluacion' => ['value' => $row->cuarentaiocho_evaluacion],
                    'field_cuarentaiocho_describe' => ['value' => $row->cuarentaiocho_describe],
                    'field_cuarentainueve_evaluacion' => ['value' => $row->cuarentainueve_evaluacion],
                    'field_cuarentainueve_describa' => ['value' => $row->cuarentainueve_describa],
                    'field_cincuenta_evaluacion' => ['value' => $row->cincuenta_evaluacion],
                    'field_cincuenta_parentesco' => ['value' => $row->cincuenta_parentesco],
                    'field_cincuenta_labor_impide_edu' => ['value' => $row->cincuenta_labor_impide_educacion],
                    'field_cincuenta_riesgo_integrida' => ['value' => $row->cincuenta_riesgo_integridad],
                    'field_cincuenta_supervisados_fam' => ['value' => $row->cincuenta_supervisados_familiar],
                    'field_cincuentaiuno_evaluacion' => ['value' => $row->cincuentaiuno_evaluacion],
                    'field_cincuentaiuno_pago_journal' => ['value' => $row->cincuentaiuno_pago_journal],
                    'field_cincuentaiuno_destajo' => ['value' => $row->cincuentaiuno_destajo],
                    'field_cincuentaidos_evaluacion' => ['value' => $row->cincuentaidos_evaluacion],
                    'field_cincuentaidos_describa' => ['value' => $row->cincuentaidos_describa],
                    'field_cincuentaitres_evaluacion' => ['value' => $row->cincuentaitres_evaluacion],
                    'field_cincuentaitres_describe' => ['value' => $row->cincuentaitres_describa],
                ]);
                $node->save();
            }
            catch(\Exception $e) {
             dpm($e->getMessage());
            }
        }
        else {
            drupal_set_message('The node ' . $row->dni . ' is not an inspeccion');
        }

    }

    public function SaveConformidad($row) {
        if ($row->type == 'conformidad') {

            $data = base64_decode(str_replace('data:image/png;base64,', '', $row->signature));
            $file = file_save_data($data, 'public://' . $row->_id . '.png', FILE_EXISTS_REPLACE);

            $node = Node::create([
                'type' => $row->type,
                'langcode' => 'en',
                'created' => REQUEST_TIME,
                'changed' => REQUEST_TIME,
                'uid' => 1,
                'title' => $row->productor,
                'field_inspeccion' => ['value' => $row->inspeccion],
                'field_parcela_nombre' => ['value' => $row->parcela],
                'field_preguntas' => ['value' => Json::encode($row->preguntas)],
                'field_productor_nombre' => ['value' => $row->productor],
                'field_firma' => [
                    'target_id' => $file->id(),
                    'alt' => 'Firma',
                    'title' => 'Firma'
                ],
            ]);
            $node->save();
        }
        else {
            drupal_set_message('The node ' . $row->dni . ' is not a conformidad');
        }

    }
}
