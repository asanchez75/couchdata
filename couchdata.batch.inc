<?php

use Drupal\couchdata\CouchDBService;
use \Drupal\couchdata\ContentService;

function couchdata_batch($node_type, &$context) {

    $couch = new CouchDBService();

    switch ($node_type) {
        case 'productor' : $view = $couch->getView('productores', 'productores'); break;
        case 'parcela' : $view = $couch->getView('parcelas', 'parcelas'); break;
        case 'observacion' : $view = $couch->getView('observacion', 'observacion'); break;
        case 'inspeccion' : $view = $couch->getView('inspeccion', 'inspeccion'); break;
        case 'conformidad' : $view = $couch->getView('conformidad', 'conformidad'); break;
    }
    //dpm($view->rows);
    foreach ($view->rows as $row) {
        switch ($node_type) {
            case 'productor' : ContentService::SaveProductor($row->value); break;
            case 'parcela' : ContentService::SaveParcela($row->value); break;
            case 'observacion' : ContentService::SaveObservacion($row->value); break;
            case 'inspeccion' : ContentService::SaveInspeccion($row->value); break;
            case 'conformidad' : ContentService::SaveConformidad($row->value); break;
        }

        $context['results'][] = $row->id;
        $context['message'] = "Processing... => " + $row->id;
    }
}

function couchdata_batch_finished_callback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
        $message = \Drupal::translation()->formatPlural(
            count($results),
            'Posts processed.', '@count posts processed.'
        );
    }
    else {
        $message = t('Finished with an error.');
    }
    drupal_set_message($message);
    //$_SESSION['disc_migrate_batch_results'] = $results;
}
